﻿namespace Telegram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SettingPage = new System.Windows.Forms.TabPage();
            this.ResultLogPage = new System.Windows.Forms.TabPage();
            this.startButton = new System.Windows.Forms.Button();
            this.HistoryCheckingPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.plmnTextBox = new System.Windows.Forms.TextBox();
            this.smscTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.imsiModuleTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumberModuleTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.imsiTargetTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumberDuplicatorTextBox = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.HistoryNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HistoryLog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HistoryDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultLogNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultLogData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultLogPhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResultLogDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.SettingPage.SuspendLayout();
            this.ResultLogPage.SuspendLayout();
            this.HistoryCheckingPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.SettingPage);
            this.tabControl1.Controls.Add(this.ResultLogPage);
            this.tabControl1.Controls.Add(this.HistoryCheckingPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(697, 507);
            this.tabControl1.TabIndex = 0;
            // 
            // SettingPage
            // 
            this.SettingPage.Controls.Add(this.groupBox3);
            this.SettingPage.Controls.Add(this.groupBox2);
            this.SettingPage.Controls.Add(this.groupBox1);
            this.SettingPage.Location = new System.Drawing.Point(4, 22);
            this.SettingPage.Name = "SettingPage";
            this.SettingPage.Padding = new System.Windows.Forms.Padding(3);
            this.SettingPage.Size = new System.Drawing.Size(689, 481);
            this.SettingPage.TabIndex = 0;
            this.SettingPage.Text = "Settings";
            this.SettingPage.UseVisualStyleBackColor = true;
            // 
            // ResultLogPage
            // 
            this.ResultLogPage.Controls.Add(this.dataGridView1);
            this.ResultLogPage.Location = new System.Drawing.Point(4, 22);
            this.ResultLogPage.Name = "ResultLogPage";
            this.ResultLogPage.Padding = new System.Windows.Forms.Padding(3);
            this.ResultLogPage.Size = new System.Drawing.Size(689, 481);
            this.ResultLogPage.TabIndex = 1;
            this.ResultLogPage.Text = "Result Log";
            this.ResultLogPage.UseVisualStyleBackColor = true;
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(715, 34);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(100, 100);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Start Listening";
            this.startButton.UseVisualStyleBackColor = true;
            // 
            // HistoryCheckingPage
            // 
            this.HistoryCheckingPage.Controls.Add(this.dataGridView2);
            this.HistoryCheckingPage.Location = new System.Drawing.Point(4, 22);
            this.HistoryCheckingPage.Name = "HistoryCheckingPage";
            this.HistoryCheckingPage.Padding = new System.Windows.Forms.Padding(3);
            this.HistoryCheckingPage.Size = new System.Drawing.Size(689, 481);
            this.HistoryCheckingPage.TabIndex = 2;
            this.HistoryCheckingPage.Text = "History Checking";
            this.HistoryCheckingPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.smscTextBox);
            this.groupBox1.Controls.Add(this.plmnTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(677, 110);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operator Settings";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.phoneNumberModuleTextBox);
            this.groupBox2.Controls.Add(this.imsiModuleTextBox);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 136);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(677, 130);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Module Decoder Setting";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.phoneNumberDuplicatorTextBox);
            this.groupBox3.Controls.Add(this.imsiTargetTextBox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(6, 288);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(677, 110);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target Phone Setting ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "PLMN operator \t:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "SMSC operator \t:";
            // 
            // plmnTextBox
            // 
            this.plmnTextBox.Location = new System.Drawing.Point(149, 23);
            this.plmnTextBox.Name = "plmnTextBox";
            this.plmnTextBox.Size = new System.Drawing.Size(170, 30);
            this.plmnTextBox.TabIndex = 2;
            // 
            // smscTextBox
            // 
            this.smscTextBox.Location = new System.Drawing.Point(149, 68);
            this.smscTextBox.Name = "smscTextBox";
            this.smscTextBox.Size = new System.Drawing.Size(170, 30);
            this.smscTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 23);
            this.label3.TabIndex = 0;
            this.label3.Text = "IMSI Module \t: \t";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(255, 23);
            this.label4.TabIndex = 1;
            this.label4.Text = "Mobile phone Number Module \t:";
            // 
            // imsiModuleTextBox
            // 
            this.imsiModuleTextBox.Location = new System.Drawing.Point(135, 23);
            this.imsiModuleTextBox.Name = "imsiModuleTextBox";
            this.imsiModuleTextBox.Size = new System.Drawing.Size(170, 30);
            this.imsiModuleTextBox.TabIndex = 2;
            // 
            // phoneNumberModuleTextBox
            // 
            this.phoneNumberModuleTextBox.Location = new System.Drawing.Point(267, 65);
            this.phoneNumberModuleTextBox.Name = "phoneNumberModuleTextBox";
            this.phoneNumberModuleTextBox.Size = new System.Drawing.Size(170, 30);
            this.phoneNumberModuleTextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(6, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(426, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "Setting Module Decoder jika format target adalah bukan nomor IMSI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(221, 23);
            this.label6.TabIndex = 0;
            this.label6.Text = "IMSI/Mobile Phone Target \t:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(276, 23);
            this.label7.TabIndex = 1;
            this.label7.Text = "Mobile phone Number Duplicator \t:";
            // 
            // imsiTargetTextBox
            // 
            this.imsiTargetTextBox.Location = new System.Drawing.Point(233, 23);
            this.imsiTargetTextBox.Name = "imsiTargetTextBox";
            this.imsiTargetTextBox.Size = new System.Drawing.Size(170, 30);
            this.imsiTargetTextBox.TabIndex = 2;
            // 
            // phoneNumberDuplicatorTextBox
            // 
            this.phoneNumberDuplicatorTextBox.Location = new System.Drawing.Point(288, 66);
            this.phoneNumberDuplicatorTextBox.Name = "phoneNumberDuplicatorTextBox";
            this.phoneNumberDuplicatorTextBox.Size = new System.Drawing.Size(170, 30);
            this.phoneNumberDuplicatorTextBox.TabIndex = 3;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ResultLogNumber,
            this.ResultLogData,
            this.ResultLogPhoneNumber,
            this.ResultLogDateTime});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(683, 475);
            this.dataGridView1.TabIndex = 0;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HistoryNumber,
            this.HistoryLog,
            this.HistoryDateTime});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 3);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(683, 475);
            this.dataGridView2.TabIndex = 0;
            // 
            // HistoryNumber
            // 
            this.HistoryNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.HistoryNumber.HeaderText = "No";
            this.HistoryNumber.Name = "HistoryNumber";
            this.HistoryNumber.ReadOnly = true;
            this.HistoryNumber.Width = 46;
            // 
            // HistoryLog
            // 
            this.HistoryLog.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.HistoryLog.HeaderText = "Log Data";
            this.HistoryLog.Name = "HistoryLog";
            this.HistoryLog.ReadOnly = true;
            // 
            // HistoryDateTime
            // 
            this.HistoryDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.HistoryDateTime.HeaderText = "Date & TIme";
            this.HistoryDateTime.Name = "HistoryDateTime";
            this.HistoryDateTime.ReadOnly = true;
            this.HistoryDateTime.Width = 91;
            // 
            // ResultLogNumber
            // 
            this.ResultLogNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ResultLogNumber.HeaderText = "No";
            this.ResultLogNumber.Name = "ResultLogNumber";
            this.ResultLogNumber.ReadOnly = true;
            this.ResultLogNumber.Width = 46;
            // 
            // ResultLogData
            // 
            this.ResultLogData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ResultLogData.HeaderText = "Data";
            this.ResultLogData.Name = "ResultLogData";
            this.ResultLogData.ReadOnly = true;
            // 
            // ResultLogPhoneNumber
            // 
            this.ResultLogPhoneNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ResultLogPhoneNumber.HeaderText = "Phone Number";
            this.ResultLogPhoneNumber.Name = "ResultLogPhoneNumber";
            this.ResultLogPhoneNumber.ReadOnly = true;
            this.ResultLogPhoneNumber.Width = 95;
            // 
            // ResultLogDateTime
            // 
            this.ResultLogDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ResultLogDateTime.HeaderText = "Date & Time";
            this.ResultLogDateTime.Name = "ResultLogDateTime";
            this.ResultLogDateTime.ReadOnly = true;
            this.ResultLogDateTime.Width = 83;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 531);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.SettingPage.ResumeLayout(false);
            this.ResultLogPage.ResumeLayout(false);
            this.HistoryCheckingPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage SettingPage;
        private System.Windows.Forms.TabPage ResultLogPage;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TabPage HistoryCheckingPage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox smscTextBox;
        private System.Windows.Forms.TextBox plmnTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox phoneNumberModuleTextBox;
        private System.Windows.Forms.TextBox imsiModuleTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox phoneNumberDuplicatorTextBox;
        private System.Windows.Forms.TextBox imsiTargetTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn HistoryDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultLogNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultLogData;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultLogPhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResultLogDateTime;
    }
}

